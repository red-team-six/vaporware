# /usr/bin/env python
# Download the twilio-python library from twilio.com/docs/libraries/python
from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
from os import environ
import requests

app = Flask(__name__)

@app.route("/sms", methods=['GET', 'POST'])
def sms_ahoy_reply():
    """Respond to incoming messages with a friendly SMS."""
    api_url_services = "https://civic-hackathon-2019.herokuapp.com/services"

    # Start our response

    message = request.form.get("Body")

    resp = MessagingResponse()
    
    resp_message = "Hello. Welcome to the HandUp Service. We are here to help you with information and services.\n\nHow can we help you? You can respond wtih words like 'housing', 'food', 'jobs', or 'show me my options'."

    if "housing" in message.lower():
        resp_message = "These are the housing services in the 46225 area: \n\nSalvation Army Ruth Lilly Women And Children's Center \n540 NORTH ALABAMA STREET \n317-637-5551 \n\nWheeler Mission Shelter For Men  \n520 EAST MARKET STREET \n317-687-6795 \n\nDayspring Center \n1537 CENTRAL AVENUE \n317-635-6780 \n\nPlease see https://red-team-six.gitlab.io/red-team-frontend for more information."
        requests.post(api_url_services + "/housing/increment")
    elif "food" in message.lower():
        resp_message = "These are the food services in the 46225 area: \n\nWheeler Mission Thrift Store \n2730 MADISON AVENUE \n317-791-9186 \n\nSaint John The Evangelist \n126 WEST GEORGIA STREET \n\nJames Garfield - Indianapolis Public School #31 \n307 LINCOLN STREET \n317-226-4231 \n\nPlease see https://red-team-six.gitlab.io/red-team-frontend for more information."
        requests.post(api_url_services + "/food/increment")
    elif "jobs" in message.lower():
        resp_message = "These are job services in the 46225 area: \n\nWorkOne Center \n4410 SHADELAND AVENUE \n\nPlease see https://red-team-six.gitlab.io/red-team-frontend for more information."
        requests.post(api_url_services + "/jobs/increment")
    elif "options" in message.lower():
        resp_message = "Here are all the available options: \n\nClothing \n\nFood \n\nHealth Care \n\nMental Health \n\nSubstance Abuse \n\nJobs \n\nRecords / Licenses \n\nTransportation \n\nYou can also visit us at: https://red-team-six.gitlab.io/red-team-frontend for more information and resources."

    requests.post(api_url_services + "/engagements/increment")

    # Add a message
    resp.message(resp_message)

    return str(resp)

if __name__ == "__main__":
    app.run(debug=True)
